function [t, y] = Heun(y0)
  t0 = 0;
  h = 1/365;
  T = 50;
  N = (T - t0)/h;
  t = t0:h:T;
  y = zeros(1,N);
  y(1) = y0;
  r = @(t) 0.2.*(1+cos(2*pi.*t));
  m = 0.1;
  K = 1000;
  f = @(t,y) r(t).*y.*(1-y./K) - m.*y;
  tiempo0 = time
  for i = 1:N
    y(i+1) = y(i) + h/4.*(f(t(i),y(i)) + 3.*f(t(i)+2/3*h, y(i)+2/3*h.*f(t(i),y(i))));
    time - tiempo0
  endfor
endfunction