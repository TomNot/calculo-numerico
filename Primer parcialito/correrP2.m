# corremos el programa
for y0=[50,200,500]
  [t, y] = casiTaylor(y0);
  plot(t, y, '-', MarkerSize = 0.1, LineWidth = 0.1);
  hold on;
endfor
hold off;
legend('y0 = 50','y0 = 200','y0 = 500');