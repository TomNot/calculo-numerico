function [t,v] = lanzamiento1(v0)
  t0 = 0;
  h = 0.01;
  t = [t0];
  v = [v0];
  m = 0.084;
  cr = 0.028;
  g = 9.81;
  f = @(t,x) -g - cr/m.*x.**2;
  i = 1
  while v(length(v)) > 0
    v = [v,0];
    t = [t, t(i) + h];
    v(i+1) = v(i) + h.*f(t(i) + h/2, v(i) + h/2.*f(t(i), v(i)));
    i += 1;
  endwhile
  t = t(1:length(t)-1)
  v = v(1:length(v)-1)
endfunction