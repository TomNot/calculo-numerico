# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 22:02:18 2019

@author: Tom
"""
import numpy as np
from matplotlib import pyplot as plt

n = 10000
T = 10
h = T/n
x0 = 1
#u = 1 - 1e-3

#def f (u, x):
#    return u + np.sin(x)

x = []
x.append(x0 + x0*h)
for i in range(n): # introduzca el método iterativo de resolución de ecuaciones
                   #diferenciales que quiera, a modo de formulita adentro del append()
    x.append(x[i]+x[i]*h) # Acá x está multiplicando a h porque f(t,x(t))=x(t),
                          #fijate que hay otro ejemplo de DNL comentado
t = np.linspace(0, T, n+1)    
plt.plot(t, x, 'r-', label = 'Solución de la ecuación')
plt.xlabel('Tiempo')
plt.ylabel('Posición')
#plt.xlim(T/2, T*3/2)
plt.legend()
plt.show()
plt.close()

