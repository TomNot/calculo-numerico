# -*- coding: utf-8 -*-
"""
Created on Mon May 20 12:29:17 2019

@author: Tom
"""
import numpy as np
#import math
def biseccion(f, a, b, tol):
    if f(a)*f(b) > 0:
        print('El intervalo no contiene una raíz de f')
        return None
    if f(a)*f(b) == 0:
        print('La raíz es a o b')
    if f(a)*f(b) < 0:
        c = (b+a)/2
#        print(c)
        if abs(f(c)) < tol:
            return c
        elif f(a)*f(c) < 0:
            return biseccion(f, a, c, tol)
        elif f(c)*f(b) < 0:
            return biseccion(f, c, b, tol)
def newton(f, df, x0, tol):
    if df(x0) != 0:
        xn = x0 - f(x0)/df(x0)
        if abs(f(xn)) < tol:
            return xn
        else:
            return newton(f, df, xn, tol)
#%%
f = lambda x: np.tan(x) - 2*x
df = lambda x: 1/(np.cos(x))**2 - 2
c = biseccion(f, -1, -1.5, 10e-5)
xn = newton(f, df, -1.5, 10e-5)
#%%