# -*- coding: utf-8 -*-
"""
Created on Wed May  1 13:40:44 2019
Cálculo Numérico
@author: Tom
"""
import numpy as np

def machineEpsilon(func=float):
    machine_epsilon = func(1)
    while func(1)+func(machine_epsilon) != func(1):
        machine_epsilon_last = machine_epsilon
        machine_epsilon = func(machine_epsilon) / func(2)
    return func(machine_epsilon_last)/2

def fl(x, beta = 10, d = 2):
    res =  round((x/beta**d),1)*beta**d   
    return res
#%% Práctica 1

#3) a)
beta = 10
d = 2 #longitud de la mantisa
epsilon = 1/2 * beta **(1-d)
x = 128.75

err = abs( (x - fl(x)) / x)
#%%
#b)
err2 = abs( (129 - 128.75 - fl( fl(129) - fl(128.75) ) ) / (129 - 128.75) )
print(err2) # da uno porque
cero = fl( fl(129) - fl(128.75) ) # esta variable da cero
#%% 
#a)
beta = 2
d = 8 #longitud de la mantisa
epsilon = 1/2 * beta **(1-d)
x = 128.75

err = abs( (x - fl(x)) / x)
#%%
#b)
err2 = abs( (129 - 128.75 - fl( fl(129, 2, 8) - fl(128.75, 2, 8), 2, 8) ) / (129 - 128.75) )
print(err2) # da uno porque
cero = fl( fl(129, 2, 8) - fl(128.75, 2, 8) ) # esta variable da cero