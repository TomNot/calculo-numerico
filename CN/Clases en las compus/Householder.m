function Q, R = Householder(A)
  N = length(A);
  Q = eye(N,N);
  for k:1(N-1)
    a = A(k:N, k)
    v = zeros(N,1);
    if a(1) != 0;
      for i=2:k;
        if a(i) == 0;
          v = a;
          v(1) = v(1) + norm(a)*sign(a(1));
          H = eye(N-(k-1), N - (k-1)) - 2.*( dot(v,v') )./ dot(v,v);
          A = H*A;
          Q = Q*H;
          endif
      endfor
    endif
  endfor
  R = A;