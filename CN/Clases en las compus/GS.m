function Q, R = GS(A)
  N = length(A);
  R = zeros(N,N);
  R(1,1) = norm(A(:,1));
  Q = zeros(N,N);
  Q(:,1) =  A(:,1)./R(1,1);
  for k = 2:N
    for i = 1:(k-1)
      R(1,k) = dot(A(:,k), Q(:,i));
      vecaux = sum( R(i,k).*Q(:,i) );
    endfor
    qkaux = A(:,k) - vecaux;
    R(k,k) = norm(qkaux);
    Q(:,k) = qkaux./R(k,k);
  endfor
endfunction