# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 18:43:06 2019

@author: Tom
"""
import numpy as np
import matplotlib.pyplot as plt

def GS(A):
    A = np.matrix(A)
#    assert np.linalg.inv(A), 'La matriz no es inversible'
    print('Tamaño de A: %i x %i'%A.shape)
    j = min(A.shape)
    h = max(A.shape)
    R = np.zeros((h, h))
    Q = np.zeros(A.shape)
    Q = np.matrix(Q)
    print('Tamaño de Q: %i x %i'%Q.shape)
    print('Tamaño de R: %i x %i'%R.shape)
    R[0,0] = np.sqrt(np.transpose(A[:,0])*A[:,0])
#    print(R[0,0], Q[:,0], A[:,0])
    Q[:,0] = A[:,0]/R[0,0]
    for k in range(1,h):
        Qaux = np.matrix(A[:,k], dtype = float)
#        print(Qaux)
        for i in range(k):
            R[i,k] = np.transpose(A[:,k])*Q[:,i]
#            print(R[i,k]*Q[:,i])
            Qaux += R[i,k]*Q[:,i]
        R[k,k] = np.transpose(Qaux)*Qaux
        Q[:,k] = Qaux/R[k,k]
#    print(j,max(A.shape))
    Q[:,2] = [[1],[0]]
    
    return [Q, R]
# No sirve
def QRextra(A):
    [Q, R] = np.linalg.qr(A)
    h = max(A.shape)
    print(len(Q))
    Q2aux = np.concatenate((Q,np.identity(len(Q))), axis = 1)
    print(Q2aux)
    Q2 = Q[:len(Q),:h+2]
    print(Q2)
#    for i in range(Q.shape[0]):
#        for j in range(Q.shape[1]):
#            Q2[i,j] = Q[i,j] 
    R2 = np.matrix(np.zeros((h,h)))
    for i in range(R.shape[0]):
        for j in range(R.shape[1]):
            R2[i,j] = R[i,j]
    return Q2, R2

def cuadradosMinimos(x, y, gr):
    x = np.array(x)
    y = np.transpose(np.matrix(y)) # Para que y sea un vector columna y pueda multiplicarlo por la matriz A**t
    assert x.shape[0] == y.shape[0], 'los arrays ingresados no tienen la misma dimensión'
    m = len(x)
    n = gr + 1
    A = np.zeros((m, n), dtype = float)
#    A[:,0] = np.transpose(np.array([1 for i in range(m)])) # No hace falta porque lo hago en el for
    for i in range(n):
        A[:,i] = x**i
    A = np.matrix(A)
#    print(A.shape, x.shape, y.shape)
#    print(A)
    res = np.linalg.solve(np.transpose(A)*A, np.transpose(A)*y) # Resuelvo la ecuación normal
    return res

def cuadradosMinimosQR(x, y, gr):
    x = np.array(x)
    y = np.transpose(np.matrix(y)) # Para que y sea un vector columna y pueda multiplicarlo por la matriz A**t
    assert x.shape[0] == y.shape[0], 'los arrays ingresados no tienen la misma dimensión'
    m = len(x)
    n = gr + 1
    A = np.zeros((m, n), dtype = float)
#    A[:,0] = np.transpose(np.array([1 for i in range(m)])) # No hace falta porque lo hago en el for
    for i in range(n):
        A[:,i] = x**i
    A = np.matrix(A)
    [Q, R] = np.linalg.qr(A)

#    print(A.shape, x.shape, y.shape)
#    print(A)
#    [Q, R] = QRextra(A)
    print(Q.shape,R.shape)
    res = np.linalg.solve(R, np.transpose(Q)*y) # Resuelvo la ecuación normal
    return res

def cuadradosMinimosDVS(x, y, gr):
    x = np.array(x)
    y = np.transpose(np.matrix(y)) # Para que y sea un vector columna y pueda multiplicarlo por la matriz A**t
    assert x.shape[0] == y.shape[0], 'los arrays ingresados no tienen la misma dimensión'
    m = len(x)
    n = gr + 1
    A = np.zeros((m, n), dtype = float)
    for i in range(n):
        A[:,i] = x**i
    A = np.matrix(A)
#    print(A.shape, x.shape, y.shape)
#    print(A)
#    res = np.linalg.solve(np.transpose(A)*A, np.transpose(A)*y) # Resuelvo la ecuación normal
    U, S, Vh = np.linalg.svd(A, full_matrices=True, compute_uv = True) # Ojo que S sólo tiene los elementos de la diagonal
#    print(U.shape, S.shape, Vh.shape)
    Si = np.zeros((Vh.shape[0], U.shape[1]), dtype=float)
    for i in range(min(Si.shape)):
        Si[i,i] = 1/S[i]
    Ai = np.transpose(Vh)*Si*np.transpose(U)
    res = Ai*y
    return res
#%%
x = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
y = [0.35, 0.5, 0.45, 0.55, 0.6, 0.1, 0.9, 0.75, 0.8, 0.8]
coef = cuadradosMinimosDVS(x, y, 1)
coefQR = cuadradosMinimosQR(x, y, 1)
print(coef)
print(coefQR)
#%%
A = np.matrix([[3, 2, 3], [4, 5, 6]])
[Q, R] = np.linalg.qr(A)
[Q1, R1] = GS(A)
print(Q, R)
#print(Q1, R1)
[Q2, R2] = QRextra(A)

 #%%
A = np.matrix([[3, 2, 3], [4, 5, 6]])
B = np.identity(2)
C = np.concatenate((A,B), axis = 1)
print(C)
