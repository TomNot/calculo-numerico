# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 20:08:36 2019

@author: Tom
"""

import numpy as np
import matplotlib.pyplot as plt

def cuadradosMinimos(x, y, gr):
    x = np.array(x)
    y = np.transpose(np.matrix(y)) # Para que y sea un vector columna y pueda multiplicarlo por la matriz A**t
    assert x.shape[0] == y.shape[0], 'los arrays ingresados no tienen la misma dimensión'
    m = len(x)
    n = gr + 1
    A = np.zeros((m, n), dtype = float)
#    A[:,0] = np.transpose(np.array([1 for i in range(m)])) # No hace falta porque lo hago en el for
    for i in range(n):
        A[:,i] = x**i
    A = np.matrix(A)
#    print(A.shape, x.shape, y.shape)
#    print(A)
    res = np.linalg.solve(np.transpose(A)*A, np.transpose(A)*y) # Resuelvo la ecuación normal
    return res

def cuadradosMinimosDVS(x, y, gr):
    x = np.array(x)
    y = np.transpose(np.matrix(y)) # Para que y sea un vector columna y pueda multiplicarlo por la matriz A**t
    assert x.shape[0] == y.shape[0], 'los arrays ingresados no tienen la misma dimensión'
    m = len(x)
    n = gr + 1
    A = np.zeros((m, n), dtype = float)
    for i in range(n):
        A[:,i] = x**i
    A = np.matrix(A)
#    print(A.shape, x.shape, y.shape)
#    print(A)
#    res = np.linalg.solve(np.transpose(A)*A, np.transpose(A)*y) # Resuelvo la ecuación normal
    U, S, Vh = np.linalg.svd(A, full_matrices=True, compute_uv = True) # Ojo que S sólo tiene los elementos de la diagonal
#    print(U.shape, S.shape, Vh.shape)
    Si = np.zeros((Vh.shape[0], U.shape[1]), dtype=float)
    for i in range(min(Si.shape)):
        Si[i,i] = 1/S[i]
    Ai = np.transpose(Vh)*Si*np.transpose(U)
    res = Ai*y
    return res
#%%
x = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
y = [0.35, 0.5, 0.45, 0.55, 0.6, 0.1, 0.9, 0.75, 0.8, 0.8]
coef = cuadradosMinimosDVS(x, y, 1)
print(coef)
#%% Ejercicio de parcial
f = lambda x: abs(x**2-4*x)
x = np.linspace(-5,5,15)
y = f(x)
coef1 = cuadradosMinimos(x,y,1)
coef3 = cuadradosMinimos(x,y,3)
coef7 = cuadradosMinimos(x,y,7)
coef9 = cuadradosMinimos(x,y,9)

def p1(x, coef1):
    n = 1
    xn = np.matrix([x**i for i in range(n + 1)])
    y = np.transpose(xn)*coef1
    return y

def p3(x, coef3):
    n = 3
    xn = np.matrix([x**i for i in range(n + 1)])
    y = np.transpose(xn)*coef3
    return y

def p7(x, coef7):
    n = 7
    xn = np.matrix([x**i for i in range(n + 1)])
    y = np.transpose(xn)*coef7
    return y

def p9(x, coef9):
    n = 9
    xn = np.matrix([x**i for i in range(n + 1)])
    y = np.transpose(xn)*coef9
    return y

plt.plot(x, p1(x, coef1), 'r*', label = 'n = 1')
plt.plot(x, p3(x, coef3), 'b.', label = 'n = 3')
plt.plot(x, p7(x, coef7), 'g^', label = 'n = 7')
plt.plot(x, p9(x, coef9), 'co', label = 'n = 9')
plt.plot(x, y, 'k-', label = 'f(x)')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 'best')
plt.grid(True)
plt.show
print('n: grado del polinomio')
