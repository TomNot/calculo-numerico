# -*- coding: utf-8 -*-
"""
Created on Sun Jul 21 18:40:43 2019

@author: Tom
"""
import numpy as np

def biseccion(f, a, b, tol):
    if f(a)*f(b) >= 0:
        print('No hay una raíz dentro del intervalo, \npodría ser que uno de los extremos sí lo sea')
        return None
    else:
        while abs(b-a) > tol:
            print(b-a)
            c = (b + a)/2
            print(c)
            if f(c) == 0:
                print('La raíz obtenida por el método de bisección es: {}' .format(c))
                return c, 0
            elif f(c)*f(b) < 0:
                a = c
            elif f(a)*f(c) < 0:
                b = c
        error = abs(b-a)
        return c, error


f = lambda x: (x-1)**3
#%%
raiz, error = biseccion(f, 0, 2.1, 10e-5)
print(raiz, error)