#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 23 12:31:00 2019

@author: tom notenson
"""
import numpy as np
from scipy.interpolate import lagrange
def DD(x,y):
    x = np.array(x)
    y = np.array(y)
    assert x.shape[0] == y.shape[0], 'los vectores ingresados no tienen la misma dimensión'
    N = x.shape[0]
    D = np.zeros((N,N))
    D[:,0] = y
    for j in range(1,N):
        for i in range(0, N-j):
            assert x[i+j] != x[i], 'j = %i y i = %i'%(j, i)
            D[i, j] = (D[i+1, j-1] - D[i, j-1])/(x[i+j] - x[i])
    return D
#%%
x = [0, 1, 3, 4]
y = [-1, 3, 2, 0]
D = DD(x,y)
print(D)
#%% 
def L(z, x, i):
    num = 1
    dem = 1    
    for j in range(len(x)):
        if j != i:
#            xaux[j] = z - x[j]
#            n[j] = x[i]-x[j]
#        print(xaux)
            num = num*(z - x[j])
            dem = dem*(x[i] - x[j])
    
    return num/dem
        
def Lagrange(z, x, y):
    x = np.array(x)
    y = np.array(y)
    assert x.shape[0] == y.shape[0], 'los vectores ingresados no tienen la misma dimensión'
    N = x.shape[0]
    res = 0
    for i in range(N):
#        print(y[i]*L(z, x, i))
        res = res + y[i]*L(z, x, i)
    return res
#%%
#import sympy
z = 16
x = [-1, 0, 2, 3]
y = [-1, 3, 11, 27]
fz = Lagrange(z, x, y)
print(fz)
poly = lagrange(x, y)
print(poly(z))
#z = sympy.Symbol('z')
#print(poly(z))
#%% 
def DDH(x, y, df):
    x = np.array(x)
    y = np.array(y)
    df = np.array(df)
    assert x.shape[0] == y.shape[0], 'los vectores ingresados no tienen la misma dimensión'
    assert x.shape[0] == df.shape[0], 'los vectores ingresados no tienen la misma dimensión'
    N = x.shape[0]
    D = np.zeros((2*N,2*N))#quizás es 2N x 2N
    z = np.repeat(x,2)
    fz = np.repeat(y,2)
    dfz = np.repeat(df,2)
    D[:,0] = fz
    print(D.shape)
    pares = [2*k for k in range(N)]
    impares = [2*k+1 for k in range(N-1)]
    for k in impares:
        D[k, 1] = (D[k+1, 0] - D[k, 0])/(z[k+1] - z[k])
    for k in pares:
        D[k, 1] = dfz[k]
    for j in range(2,2*N+2):
        for i in range(0, 2*N-j):
            assert z[i+j] != z[i], 'j = %i y i = %i'%(j, i)
            D[i, j] = (D[i+1, j-1] - D[i, j-1])/(z[i+j] - z[i])
    return D

def verD(D):
    for i in range(len(D)):
        print('\n')
        print(D[i,:])
#%%
fun = lambda x: np.sin(np.pi/2*x)
x = np.array([-1, 0, 1, 1])
#y = [-1, 3, 2, 0]
y = fun(x)
#df = [1] + [(y[i+1]-y[i])/(x[i+1]-x[i]) for i in range(len(x)-1)]
#df = [0, 0, 0, 0]
#D = DDH(x,y,df)
#verD(D)
#%%
N = x.shape[0]
from numpy.polynomial import hermite
#Dteo = hermite.hermfit(x, y, 2*N - 1)
#print(Dteo)
Dteo = hermite.hermfit(x, y, 2*N - 1)
print(Dteo)

#%% 
def puntoFijo(g, a, b, c, tol):
    f = lambda x: g(x) - x
    if f(a)*f(b) > 0:
        print('No hay raíces en el intervalo')
    if f(a)*f(b) == 0:
        print('la raiz está en los extremos')
    if abs(f(c)) < tol:
        return c
    c = g(c)
    if f(a)*f(c) < 0:
        print(c)
        return puntoFijo(g, a, c, c, tol)
    if f(c)*f(b) < 0:
        print(c)
        return puntoFijo(g, c, b, c, tol)
#%%
g = lambda x: 2**(-x)
f = lambda x: g(x) - x
c = puntoFijo(g, 0, 1, 0.5, 10e-5)
#%%
import matplotlib.pyplot as plt
x = np.linspace(-25, 25, 51)
y = 5*x

plt.plot(x, np.exp(-x), 'k-', label = 'exp(-x)')
plt.plot(x, y, 'b-', label = '5x')
plt.ylim(-1, 10)
plt.xlim(-5, 2)
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 'best')
plt.grid(True)
plt.show