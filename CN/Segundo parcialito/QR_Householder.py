# -*- coding: utf-8 -*-
"""
Created on Sun Jul 21 17:39:47 2019

@author: Tom
"""
import numpy as np
import copy

def householderQR(A):
    N = len(A)
    A = np.matrix(A)
    Q = np.identity(N) #np.matrix([[1 for i in range(N)] for j in range(N)])
    for k in range(N-1):
        a = copy.deepcopy(A[k:N, k])
#        print(A)
#        print(a.shape)
        flag = False
        for l in range(1, len(a)):
            if a[l] != 0:
                flag = True
        if flag:
            v = a
            if a[0] < 0:
                v[0] = v[0] + np.sqrt(np.dot(np.transpose(a),a))*(-1)
            else:
                v[0] = v[0] + np.sqrt(np.dot(np.transpose(a),a))
            Haux = np.identity(N-k) - 2*v*np.transpose(v)/np.dot(np.transpose(v),v) #np.matrix([[1 for i in range(N-k)] for j in range(N-k)])
            print('Haux')
            print(Haux)
            print('A')
            print(A)
            H = np.identity(N)
            H[k:N, k:N] = Haux
            print('H')
            print(H)
            A = np.dot(H, A)
            Q = np.dot(Q, H)
            print('A')
            print(A)
            print('Q')
            print(Q)
    R = A
    return Q, R

#%% 
A = np.transpose(np.matrix([[0, 2, 0], [np.sqrt(3)/2, 1, 1/2]]))
Q, R = householderQR(A)
print('A')
print(A)
print('Q y R')
print(Q, R)