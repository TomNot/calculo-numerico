# -*- coding: utf-8 -*-
"""
Created on Sat Jul 20 19:25:13 2019

@author: Tom
"""

import numpy as np
import matplotlib.pyplot as plt

def cuadradosMinimos(x, y, gr):
    x = np.array(x)
    y = np.transpose(np.matrix(y))
#    print(x.shape[0] == y.shape[0])
    assert x.shape[0] == y.shape[0], 'Los tamaños de los x e y ingresados no coinciden'
    A = vandermonde(x, gr)
    res = np.linalg.solve(np.transpose(A)*A, np.transpose(A)*y) # Resuelvo la ecuación normal
    return res
    
def vandermonde(x, gr):
    m = x.shape[0]
    A = np.zeros((m, gr+1))
#    print(A.shape)
    for i in range(gr+1):
        A[:,i] = x**i
    A = np.matrix(A)
    return A
#%%
f = lambda x: x**2
def p1(x, coef):
    aux = 0
    for i in range(len(coef)):
        aux += coef[i-1]*x**i 
    res = [aux[0, i] for i in range(aux.shape[1])]
    return res

x = np.linspace(-1, 1, 20)#, endpoint = True)
y = f(x)
gr = 1
coef = cuadradosMinimos(x, y, gr)


plt.plot(x, p1(x, coef), 'r*', label = 'n = 1')
plt.plot(x, y, 'k-', label = 'f(x)')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 'best')
plt.grid(True)
plt.show
#print('n: grado del polinomio')


    