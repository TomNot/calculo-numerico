function [t, y] = casiTaylor(y0)
  t0 = 0;
  h = 1/365;
  T = 50;
  N = (T - t0)/h;
  t = t0:h:T;
  y = zeros(1,N);
  y(1) = y0;
  alfa = 0.2;
  K = 1000;
  f = @(t,y) alfa.*y.*(1-y./K) - y.**2/(1+y.**2) ;
  tiempo0 = time
  for i = 1:N
    Ci = ( f(t(i), y(i) + h) - f(t(i), y(i)) )./h;
    y(i+1) = y(i) + h.*f(t(i),y(i)) + h**2/2.*( Ci.*f(t(i),y(i)) );
    time - tiempo0
  endfor
endfunction