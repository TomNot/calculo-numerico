function [t,v]=lanzamiento(vo)
g=9.81;
m=0.084;
cr=0.028;
h=0.005;
N=200;

f=@(v) -g-(cr/m)*(v^2);

v=zeros(1,N);
v(1)=vo;
for i=1:N-1
    v(i+1)=v(i)+h*f(v(i)+(h/2)*f(v(i)));
end

l=max(find(v>=0));
v=v(1:l);
t=zeros(1,length(v));
for i=1:length(v)-1
    t(i+1)=t(i)+h/2;
end

end

