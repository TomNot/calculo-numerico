# corremos el programa
for y0=[100,500,1000]
  [t, y] = Heun(y0);
  hold on;
  Leyenda=strcat('CI=',num2str(y0));
  plot(t, y, "-", MarkerSize = 0.1, LineWidth = 0.1, 'DisplayName', label = Leyenda)
  xlabel ("t: tiempo en a�os");
  ylabel ("y (t): cantidad de individuos");
  title ("Heun");
  legend;
endfor
#legend('y0 = 100','y0 = 500','y0 = 1000');
hold off;